<?php
	require_once 'init/db.php';
	require_once 'init/dQuery.php';
	require_once 'helpers/helpers.php';
	include_once 'partials/head.php';
?>

<?php
	// import head section
	include_once 'partials/header.php';
	include_once 'partials/modal.php';

?>

<main>
	<div id="error" class="message bg-warning">
		<?=((isset($_SESSION['error']))?$_SESSION['error']:''); ?>
	</div><!-- View Errors -->
	<div id="result" class="message bg-success">
		<?=((isset($_SESSION['success']))?$_SESSION['success']:''); ?>
	</div><!-- View valid Post -->
	<h1>Тук можете да регистрирате организациите!</h1>
	<form action="core/addDatatoDB.php?form=organization" method="post">
		<div class="row input-div">
				<div class="col-2">
					<input type="text" name="name" class="form-control" placeholder="Име">
				</div>
				<div class="col-3">
					<input type="text" name="address" class="form-control" placeholder="Адрес">
				</div>
				<input type="submit" name="submit" value="Submit" class="btn btn-success button">
		</div>
	</form>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Име</th>
				<th>Адрес</th>
				<th>Edit/Delete</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$result = getTableDb('organization');
				$i = 1;
				while ($organization = mysqli_fetch_assoc($result)):
			?>
			<tr>
				<th scope="row"><?=$i++; ?></th>
				<td><?=$organization['Ime']; ?></td>
				<td><?=$organization['Adres']; ?></td>
				<td>
					<a id="edit" href="ajax/userAction.php?table=organization&action=edit&key=<?=$organization['O_ID'];?>" class="operation fa fa-pencil-square-o" title="Edit" data-toggle="modal" data-target="#myModal"></a>
					<a id="delete" href="ajax/userAction.php?table=organization&action=delete&key=<?=$organization['O_ID'];?>" class="operation fa fa-trash" title="Remove"></a>
				</td>
			</tr>
			<?php endwhile;	?>
		</tbody>
	</table>
</main>
<?php
	// import footer.
	include_once 'partials/footer.php';

?>
