<?php
	require_once 'init/db.php';
	require_once 'init/dQuery.php';
	require_once 'helpers/helpers.php';
	include_once 'partials/head.php';
?>

<!-- CSS goes here -->

<?php
	include_once 'partials/header.php';
	include_once 'partials/modal.php';

 ?>
<!-- container goes here. -->

<main>
	<div id="error" class="message bg-warning">
		<?=((isset($_SESSION['error']))?$_SESSION['error']:''); ?>
	</div><!-- View Errors -->
	<div id="result" class="message bg-success">
		<?=((isset($_SESSION['success']))?$_SESSION['success']:''); ?>
	</div><!-- View valid Post -->
			<h1>Тук можете да регистрирате лекциите!</h1>
				<form action="core/addDatatoDB.php?form=lectures" method="post">
					<div class="row input-div">
						<div class="col-3">
							<input type="text" name="subject" class="form-control" placeholder="Тема">
						</div>
						<div class="col-3">
							<input type="text" name="duration" class="form-control" placeholder="Продължителост">
						</div>
						<input type="submit" class="btn btn-success button" name="submit" value="Submit">
					</div>
				</form>

				<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Тема</th>
					<th>Продължителност</th>
					<th>Edit/Delete</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$result = getTableDb('lectures');
					$i = 1;
					while ($lectures = mysqli_fetch_assoc($result)):
				?>
				<tr>
					<th scope="row"><?=$i++; ?></th>
					<td><?=$lectures['Tema']; ?></td>
					<td><?=getTime($lectures['DuljinaL']); ?></td>
					<td>
						<a id="edit" href="ajax/userAction.php?table=lectures&action=edit&key=<?=$lectures['L_ID'];?>" class="operation fa fa-pencil-square-o" title="Edit" data-toggle="modal" data-target="#myModal"></a>
						<a id="delete" href="ajax/userAction.php?table=lectures&action=delete&key=<?=$lectures['L_ID'];?>" class="operation fa fa-trash" title="Remove"></a>
					</td>
				</tr>
				<?php endwhile;	?>
			</tbody>
		</table>
</main>
<?php
	// import footer.
	include_once 'partials/footer.php';


?>
