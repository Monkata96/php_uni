<?php
	require_once 'init/dQuery.php';
	// import head section
	include_once 'partials/head.php';

?>

<!-- CSS goes here -->
<link rel="stylesheet" href="css/lecturers.css">

<?php
	include_once 'partials/header.php';
	include_once 'partials/modal.php';

 ?>
<!-- container goes here. -->
<main>
	<div id="error" class="message bg-warning">
		<?=((isset($_SESSION['error']))?$_SESSION['error']:''); ?>
	</div><!-- View Errors -->
	<div id="result" class="message bg-success">
		<?=((isset($_SESSION['success']))?$_SESSION['success']:''); ?>
	</div><!-- View valid Post -->
	<h1>Тук можете да регистрирате преподавателите!</h1>
	<form class="form" id="registerLecturers" action="core/addDatatoDB.php?form=lecturers" method="post">
		<div class="row input-div">
			<div class="col-3">
				<input type="text" name="name" class="form-control" placeholder="Име на преподавател">
			</div>
			<div class="col-3">
				<input type="text" name="speciality" class="form-control" placeholder="Специалност">
			</div>
			<div class="col-2.5 input-width">
				<select name="workplace" id="" class="form-control">
					<option value="">Избери Университет</option>
					<?php
						require_once 'init/db.php';
						$sql = "SELECT * FROM organization";
						$result = $conn->query($sql);
						while ($organization = mysqli_fetch_assoc($result)):
					?>
						<option value="<?=$organization['Ime']; ?>"><?=$organization['Ime']; ?></option>
					<?php endwhile;	?>
				</select>
			</div>
			<input type="submit" name="submit" value="Submit" class="btn btn-success button">
		</div>
	</form>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Име</th>
				<th>Специалност</th>
				<th>Работно място</th>
				<th>Edit/Delete</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$result = getTableDb('lecturers');
				$i = 1;
				while ($lecturers = mysqli_fetch_assoc($result)):
			?>
			<tr>
				<th scope="row"><?=$i++; ?></th>
				<td><?=$lecturers['NameOfLect']; ?></td>
				<td><?=$lecturers['Speciality']; ?></td>
				<td><?=$lecturers['Workplace']; ?></td>
				<td>
					<a id="edit" href="ajax/userAction.php?table=lecturers&action=edit&key=<?=$lecturers['Lec_ID'];?>" class="operation fa fa-pencil-square-o" title="Edit" data-toggle="modal" data-target="#myModal"></a>
					<a id="delete" href="ajax/userAction.php?table=lecturers&action=delete&key=<?=$lecturers['Lec_ID'];?>" class="operation fa fa-trash" title="Remove"></a>
				</td>
			</tr>
			<?php endwhile;	?>

		</tbody>
	</table>
</main>

<?php
	// import footer.
	include_once 'partials/footer.php';
?>
