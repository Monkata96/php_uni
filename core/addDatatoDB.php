<!-- Adding data to Database. used on all pages. -->
<?php
	session_start();
	require_once '../init/db.php';
	require_once '../helpers/helpers.php';
	$fields = "";
	$values = "";
	$sql = "";

	$form = $_GET['form'];


	if (isset($_POST['submit'])) {
		if ($form === "lecturers") {
			$name = $_POST['name'];
			$speciality = $_POST['speciality'];
			$workplace = $_POST['workplace'];
			$fields = "(NameOfLect, Speciality, Workplace)";
			$values = "('$name', '$speciality', '$workplace')";
			if (!empty($name) && !empty($speciality) && !empty($workplace)) {
				$sql = "INSERT INTO lecturers $fields VALUES $values";
			} else {
				$_SESSION["error"] = "Fields can't be empty!!!";
			}
		}

		if ($form === "lectures") {
			$subject = $_POST['subject'];
			$duration = $_POST['duration'];
			$fields = "(Tema, DuljinaL)";
			$values = "('$subject', '$duration')";
			if (!empty($subject) && !empty($duration)) {
				$sql = "INSERT INTO lectures $fields VALUES $values";
			} else {
				$_SESSION["error"] = "Fields can't be empty!!!";
			}
		}

		if ($form === "organization") {
			$name = $_POST['name'];
			$address = $_POST['address'];
			$fields = "(Ime, Adres)";
			$values = "('$name', '$address')";
			if (!empty($name) && !empty($address)) {
				$sql = "INSERT INTO organization $fields VALUES $values";
			} else {
				$_SESSION["error"] = "Fields can't be empty!!!";
			}
		}

		if ($form === "guests") {
			$organization = $_POST['organization'];
			$lecturer = $_POST['lecturer'];
			$subject = $_POST['subject'];
			$CountGuests = $_POST['guests'];
			$time = $_POST['time'];
			$date = $_POST['date'];

			$fields = "(O_ID, L_ID, BrojG, Lec_ID, Date, time)";
			$values = "('$organization', '$subject', '$CountGuests', '$lecturer', '$date', '$time')";
			if (!empty($organization) && !empty($subject) && !empty($CountGuests) && !empty($lecturer) && !empty($date) && !empty($time)) {
				$sql = "INSERT INTO guests $fields VALUES $values";
			} else {
				$_SESSION["error"] = "Fields can't be empty!!!";
			}
		}

		if (!empty($sql)) {
			var_dump($sql);
			$result = $conn->query($sql);
			if ($result) {
				$_SESSION["success"] = "Database updated.";
			}else {
				$_SESSION["error"] = "Error processing form.";
			}
		}
	}
	header("Location: {$_SERVER['HTTP_REFERER']}");
?>
