<!-- Updating table data to Database -->
<?php
session_start();
require_once '../init/db.php';
require_once '../helpers/helpers.php';
$sql = "";
$selector = array("guests"=>"id", "lecturers"=>"Lec_ID", "lectures"=>"L_ID", "organization"=>"O_ID");
$id = $_POST['id'];
$form = $_GET['form'];

	if ($form === "lecturers") {
		$name = $_POST['name'];
		$speciality = $_POST['speciality'];
		$workplace = $_POST['workplace'];

		if (!empty($name) && !empty($speciality) && !empty($workplace)) {
			$sql = "UPDATE lecturers SET
						NameOfLect = '$name',
						Speciality = '$speciality',
						Workplace = '$workplace'
						WHERE $selector[$form] = " .$id;
		} else {
			$_SESSION["error"] = "Fields can not be empty";
		}
	}

	if ($form === "lectures") {
		$subject = $_POST['subject'];
		$duration = $_POST['duration'];

		if (!empty($subject) && !empty($duration)) {
			$sql = "UPDATE lectures SET
						Tema = '$subject',
						DuljinaL = '$duration'
						WHERE $selector[$form] = " .$id;
		} else {
			$_SESSION["error"] = "Fields can't be empty!!!";
		}
	}

	if ($form === "organization") {
		$name = $_POST['name'];
		$address = $_POST['address'];

		if (!empty($name) && !empty($address)) {
			$sql = "UPDATE organization SET
						Ime = '$name',
						Adres = '$address'
						WHERE $selector[$form] = " .$id;
		} else {
			$_SESSION["error"] = "Fields can't be empty!!!";
		}
	}

	if ($form === "guests") {
		$organization = $_POST['organization'];
		$lecturer = $_POST['lecturer'];
		$subject = $_POST['subject'];
		$CountGuests = $_POST['guests'];
		$time = $_POST['time'];
		$date = $_POST['date'];

		$fields = "(O_ID, L_ID, BrojG, Lec_ID, Date, time)";
		$values = "('$organization', '$subject', '$CountGuests', '$lecturer', '$date', '$time')";
		if (!empty($organization) && !empty($subject) && !empty($CountGuests) && !empty($lecturer) && !empty($date) && !empty($time)) {
			$sql = "UPDATE guests SET
						O_ID = '$organization',
						L_ID = '$subject',
						BrojG = '$CountGuests',
						Lec_ID = '$lecturer',
						Date = '$date',
						time = '$time'
						WHERE $selector[$form] = " .$id;
		} else {
			$_SESSION["error"] = "Fields can't be empty!!!";
		}
	}

	if (!empty($sql)) {
		var_dump($sql);
		$result = $conn->query($sql);
		if ($result) {
			$_SESSION["success"] = "Database updated.";
		}else {
			$_SESSION["error"] = "Error processing form." . mysqli_error($conn);
		}
	}

 header("Location: {$_SERVER['HTTP_REFERER']}");

 ?>
