$( document ).ready(function() {
	// $register = $('#registerLecturers');
	$error = $('#error');
	$result = $('#result');
	$table = $('.table').find('tbody');
	$organizationOption = $('#organization');

	$flashStatus = $('.message');

	$flashStatus.delay(5000).fadeOut(300);

	updateUI();

    // When Edit or Delete button is clicked this block runs. 
	$userAction.on('click',function(e) {
		$selector = $(this).attr('id')
		if ($selector === 'edit') {
			userAction.bind($(this))(e);
			$("#myModal").modal();
		}
	});

	// select option university
	$organizationOption.on('change',organizationOption );
	function organizationOption() {
		$val = $(this).find('option:selected').text();
		$.ajax({
			type: 'GET',
			url: 'ajax/getData.php?lecturers='+$val,
			success: function (response) {
				$('#lecturers').html(response);
			},
		});

	}

	function userAction(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		var form = UrlParams(url, 'table');
		var formName = form;
		var action = UrlParams(url, 'action');
		var key = UrlParams(url, 'key');
		$.ajax({
			type: 'GET',
			url: 'ajax/renderForm.php?form=' + form + '&key=' + key,
			success: function (form) {
				$(".modal-body").html('');
				$(".modal-body").html(form);
				$("#myModalLabel").text(formatFormName(formName));
				$organizationOption = $('#organization');
				$organizationOption.on('change',organizationOption );
			}
		});
	}
		function updateUI() {
			$userAction = $('.operation');
			$organizationOption = $('#organization');
		}
});
