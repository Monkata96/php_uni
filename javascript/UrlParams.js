// gets parameters from URL
function UrlParams(url, key) {
	var Params = url.split(/[?&]+/);
	for (Param of Params) {
		var value = Param.split('=');

		if (value[0] === key) {
			return value[1];
		}
	}
	return;
}

// Header on the Modal with EDIT + Current Page
function formatFormName(name) {
	return "EDIT " + name.toUpperCase();
}
