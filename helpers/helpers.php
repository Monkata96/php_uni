<?php

    // Used to avoid sql injections.
	function sanitize($conn, $dirty){
		global $conn;
		return mysqli_escape_string($conn, $dirty);
	}

    // formats time from M to HH:MM
	function getTime($time)
	{
		if ($time / 60 >= 1) {
            if ($time%60 == 0 ) {
                return floor($time / 60) . ' Ч.';
            } else {
                return floor($time / 60) . ' Ч ' . $time % 60 . ' Мин.';
            }
		}
		else {
			return $time . ' Мин.';
		}
	}

	function capitalizeNstrong($word) {
		return '<strong>' . strtoupper($word) . '</strong>';
	}

    function getColumnName($key) {
        $table = array("id"=>"Гост#",
                        "O_ID"=>"Организация#",
                        "L_ID"=>"Тема#",
                        "Tema"=>"Тема",
                        "BrojG"=>"Брой гости",
                        "Lec_ID"=>"Преподавател#",
                        "Date"=>"Дата",
                        "time"=>"Време",
                        "NameOfLect"=>"Име на Преподавател",
                        "Speciality"=>"Специалност",
                        "Workplace"=>"Работно място",
                        "DuljinaL"=>"Продължителност",
                        "Ime"=>"Име на Организация",
                        "Adres"=>"Адрес"
                    );

        return $table[$key];
    }
 ?>
