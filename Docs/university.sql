-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2017 at 06:10 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `university`
--

-- --------------------------------------------------------

--
-- Table structure for table `guests`
--

CREATE TABLE `guests` (
  `id` int(5) NOT NULL,
  `O_ID` tinyint(2) NOT NULL,
  `L_ID` tinyint(2) NOT NULL,
  `BrojG` tinyint(2) NOT NULL,
  `Lec_ID` tinyint(2) NOT NULL,
  `Date` date DEFAULT NULL,
  `time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_bulgarian_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `guests`
--

INSERT INTO `guests` (`id`, `O_ID`, `L_ID`, `BrojG`, `Lec_ID`, `Date`, `time`) VALUES
(23, 35, 14, 15, 115, '2017-12-18', '00:00:00'),
(24, 36, 18, 10, 119, '2017-12-02', '09:00:00'),
(25, 39, 15, 50, 117, '2018-01-01', '13:00:00'),
(26, 37, 16, 20, 118, '2017-12-20', '14:00:00'),
(27, 35, 13, 19, 114, '2017-12-22', '13:00:00'),
(28, 38, 19, 25, 116, '2018-03-01', '13:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `lecturers`
--

CREATE TABLE `lecturers` (
  `Lec_ID` tinyint(2) UNSIGNED NOT NULL,
  `NameOfLect` varchar(50) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `Speciality` varchar(30) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci DEFAULT NULL,
  `Workplace` varchar(50) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lecturers`
--

INSERT INTO `lecturers` (`Lec_ID`, `NameOfLect`, `Speciality`, `Workplace`) VALUES
(114, 'Румен Русев', 'ООП', 'Русенски университет'),
(115, 'Савка Калинова', 'Информационни системи', 'Русенски университет'),
(116, 'Симеон Валериев', 'Икономика', 'УНСС'),
(117, 'Усман Гани Кхан', 'Български език', 'Cambridge university'),
(118, 'Христо Радев', 'Изкуство', 'Нов Български университет'),
(119, 'Христо Христов', 'Кино и музика', 'Софийски университет');

-- --------------------------------------------------------

--
-- Table structure for table `lectures`
--

CREATE TABLE `lectures` (
  `L_ID` tinyint(2) UNSIGNED NOT NULL,
  `Tema` varchar(20) COLLATE cp1251_bulgarian_ci NOT NULL,
  `DuljinaL` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_bulgarian_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lectures`
--

INSERT INTO `lectures` (`L_ID`, `Tema`, `DuljinaL`) VALUES
(13, 'Масиви', 90),
(14, 'PHP', 120),
(15, 'Членуване', 45),
(16, 'Рисуване на 2D графи', 150),
(18, 'Световна режисура', 120),
(19, 'Административно дело', 150);

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `O_ID` tinyint(2) UNSIGNED NOT NULL,
  `Ime` varchar(40) COLLATE cp1251_bulgarian_ci NOT NULL,
  `Adres` varchar(40) COLLATE cp1251_bulgarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_bulgarian_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`O_ID`, `Ime`, `Adres`) VALUES
(35, 'Русенски университет', 'Русе'),
(36, 'Софийски университет', 'София'),
(37, 'Нов Български университет', 'София'),
(38, 'УНСС', 'София'),
(39, 'Cambridge university', 'London');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guests`
--
ALTER TABLE `guests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecturers`
--
ALTER TABLE `lecturers`
  ADD PRIMARY KEY (`Lec_ID`);

--
-- Indexes for table `lectures`
--
ALTER TABLE `lectures`
  ADD PRIMARY KEY (`L_ID`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`O_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guests`
--
ALTER TABLE `guests`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `lecturers`
--
ALTER TABLE `lecturers`
  MODIFY `Lec_ID` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT for table `lectures`
--
ALTER TABLE `lectures`
  MODIFY `L_ID` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `O_ID` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
