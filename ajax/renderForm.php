<!-- Renders form when clicked on edit button on any page. -->
<?php

	require_once '../init/db.php';

	$form = $_GET['form'];
	$key = $_GET['key'];

	$selector = array("guests"=>"id", "lecturers"=>"Lec_ID", "lectures"=>"L_ID", "organization"=>"O_ID");
	$sql = "SELECT * FROM $form WHERE $selector[$form] = $key";

	$conn->query($sql);
	$result = $conn->query($sql);

	$data = mysqli_fetch_assoc($result);

	if ($form === 'lectures') {
	?>
	<form action="core/editForm.php?form=lectures" method="post">
		<div class="row input-div">
			<input type="hidden" name="id" value="<?=$key;?>"></input>
			<div class="col-12">
				<input type="text" name="subject" class="form-control" placeholder="Тема" value="<?=$data['Tema'];?>" >
			</div>
			<br>
			<div class="col-12">
				<input type="text" name="duration" class="form-control" placeholder="Продължителост" value="<?=$data['DuljinaL'];?>">
			</div>
			<br>
			<div class="col-12">
				<input type="submit" class="btn btn-primary pull-right" value="Save changes">
			</div>

		</div>
	</form>
	<?php
	}
	if ($form === 'lecturers') {
	?>
	<form class="form" action="core/editForm.php?form=lecturers" method="post">
		<div class="row input-div">
			<input type="hidden" name="id" value="<?=$key;?>"></input>
			<div class="col-12">
				<input type="text" name="name" class="form-control" placeholder="Име на преподавател" value="<?=$data['NameOfLect'];?>" >
			</div>
			<br>
			<div class="col-12">
				<input type="text" name="speciality" class="form-control" placeholder="Специалност" value="<?=$data['Speciality'];?>" >
			</div>
			<br>
			<div class="col-12 input-width">
				<select name="workplace" id="" class="form-control" placeholder="Име на организацията">
					<option value="<?=$data['Workplace'];?>" selected="selected"><?=$data['Workplace'];?></option>
					<?php
						require_once '../init/db.php';
						$sql = "SELECT * FROM organization";
						$result = $conn->query($sql);
						$count = mysqli_num_rows($result);
						$i = 1;
						while ($organization = mysqli_fetch_assoc($result)):
					?>
						<option value="<?=$organization['Ime']; ?>"><?=$organization['Ime']; ?></option>
					<?php endwhile;	?>
				</select>
			</div>
			<br>
			<div class="col-12">
				<input type="submit" class="btn btn-primary pull-right" value="Save changes">
			</div>
		</div>
	</form>
	<?php
	}
	if ($form === 'organization') {
	?>
	<form action="core/editForm.php?form=organization" method="post">
		<div class="row input-div">
			<input type="hidden" name="id" value="<?=$key;?>"></input>
			<div class="col-12">
				<input type="text" name="name" class="form-control" placeholder="Име" value="<?=$data['Ime'];?>" >
			</div>
			<br>
			<div class="col-12">
				<input type="text" name="address" class="form-control" placeholder="Адрес" value="<?=$data['Adres'];?>" >
			</div>
			<br>
			<div class="col-12">
				<input type="submit" class="btn btn-primary pull-right" value="Save changes">
			</div>
		</div>
	</form>
	<?php
	}
	if ($form === 'guests') {
	?>
	<form action="core/editForm.php?form=guests" method="post">
		<div class="row input-div">
			<input type="hidden" name="id" value="<?=$key;?>"></input>
				<div class="col-12">
					<select name="organization" id="organization" class="form-control" placeholder="Име на организацията">
						<?php
							$sql = "SELECT * FROM organization where O_ID =" .$data['O_ID'];
							$result = $conn->query($sql);
							$orgData = mysqli_fetch_assoc($result)
						 ?>
						<option value="<?=$orgData['O_ID'];?>" selected="selected"><?=$orgData['Ime'];?></option>
						<?php
							require_once '../init/db.php';
							$sql = "SELECT * FROM organization";
							$result = $conn->query($sql);
							$count = mysqli_num_rows($result);
							while ($organization = mysqli_fetch_assoc($result)):
						?>
							<option value="<?=$organization['O_ID'];?>"><?=$organization['Ime']; ?></option>
						<?php endwhile;	?>
					</select>
				</div>
				<br>
				<div class="col-12">
					<select name="lecturer" id="lecturers" class="form-control">
						<option>Избери Преподавател</option>
					</select>
				</div>
				<br>
				<div class="col-12">
					<select name="subject" id="subject" class="form-control" value="<?=$data['Lec_ID'];?>" >
						<option value="">Избери Тема</option>
						<?php
							require_once '../init/db.php';
							$sql = "SELECT * FROM lectures";
							$result = $conn->query($sql);
							while ($subjects = mysqli_fetch_assoc($result)):
						?>
							<option value="<?=$subjects['L_ID'];?>"><?=$subjects['Tema']; ?></option>
						<?php endwhile;	?>
					</select>
				</div>
				<br>
				<div class="col-12">
					<input type="number" name="guests" class="form-control" placeholder="Брой гости" min="0" value="<?=$data['BrojG'];?>" >
				</div>
				<br>
				<div class="col-12">
					<input type="time" name="time" class="form-control" placeholder="Време" value="<?=$data['time'];?>" >
				</div>
				<br>
				<div class="col-12">
					<input type="date" name="date" class="form-control" placeholder="Дата" min="2017-01-02" value="<?=$data['Date'];?>" >
				</div>
				<br>
				<div class="col-12">
					<input type="submit" class="btn btn-primary pull-right" value="Save changes">
				</div>
			</div>
		</form>
	<?php
	}

 ?>
