<!-- Retrieves data from database and shows them on  each page. -->
<?php
	function getData($table){
		require_once '../init/db.php';
		$sql = "SELECT * FROM ".$table;
		$result = $conn->query($sql);
		$count = mysqli_num_rows($result);
		$i = 1;
		while ($data = mysqli_fetch_assoc($result)):
		?>
		<?php if ($table == 'lecturers'): ?>
			<tr>
				<th scope="row"><?=$i++; ?></th>
				<td><?=$data['NameOfLect']; ?></td>
				<td><?=$data['Speciality']; ?></td>
				<td><?=$data['Workplace']; ?></td>
				<td>
					<a id="edit" href="ajax/userAction.php?table=lecturers&action=edit&key=<?=$data['Lec_ID'];?>" class="operation fa fa-pencil-square-o" title="Edit" data-toggle="modal" data-target="#myModal"></a>
					<a id="delete" href="ajax/userAction.php?table=lecturers&action=delete&key=<?=$data['Lec_ID'];?>" class="operation fa fa-trash" title="Remove"></a>
				</td>
			</tr>
		<?php endif; ?>

		<?php if ($table == 'guests'): ?>
			<tr>
				<th scope="row"><?=$i++; ?></th>
				<td><?php
					$sql = "SELECT Ime FROM organization where O_ID = ".$guests['O_ID'];
					$orgName = mysqli_fetch_assoc($conn->query($sql));
					echo $orgName['Ime'];
				 ?></td>
				<td><?php
					$sql = "SELECT NameOfLect FROM lecturers where Lec_ID = ".$guests['Lec_ID'];
					$LecName = mysqli_fetch_assoc($conn->query($sql));
					echo $LecName['NameOfLect'];
				 ?></td>
				<td><?php
					$sql = "SELECT Tema FROM lectures where L_ID = ".$guests['L_ID'];
					$LecName = mysqli_fetch_assoc($conn->query($sql));
					echo $LecName['Tema'];
				 ?></td>
				<td><?=$guests['BrojG']; ?></td>
				<td><?=$guests['time']; ?></td>
				<td><?=$guests['Date']; ?></td>
				<td>
					<a id="edit" href="ajax/userAction.php?table=guests&action=edit&key=<?=$guests['id'];?>" class="operation fa fa-pencil-square-o" title="Edit" data-toggle="modal" data-target="#myModal"></a>
					<a id="delete" href="ajax/userAction.php?table=guests&action=delete&key=<?=$guests['id'];?>" class="operation fa fa-trash" title="Remove"></a>
				</td>
			</tr>
		<?php endif; ?>
		<?php if ($table == 'lectures'): ?>
			<tr>
				<th scope="row"><?=$i++; ?></th>
				<td><?=$lectures['Tema']; ?></td>
				<td><?=$lectures['DuljinaL']; ?></td>
				<td>
					<a id="edit" href="ajax/userAction.php?table=lectures&action=edit&key=<?=$lectures['L_ID'];?>" class="operation fa fa-pencil-square-o" title="Edit" data-toggle="modal" data-target="#myModal"></a>
					<a id="delete" href="ajax/userAction.php?table=lectures&action=delete&key=<?=$lectures['L_ID'];?>" class="operation fa fa-trash" title="Remove"></a>
				</td>
			</tr>
		<?php endif; ?>
		<?php if ($table === 'organization'): ?>
			<tr>
				<th scope="row"><?=$i++; ?></th>
				<td><?=$data['Ime']; ?></td>
				<td><?=$data['Adres']; ?></td>
				<td>
					<a id="edit" href="ajax/userAction.php?table=organization&action=edit&key=<?=$data['O_ID'];?>" class="operation fa fa-pencil-square-o" title="Edit" data-toggle="modal" data-target="#myModal"><?=$data['O_ID'];?></a>
					<a id="delete" href="ajax/userAction.php?table=organization&action=delete&key=<?=$data['O_ID'];?>" class="operation fa fa-trash" title="Remove"></a>
				</td>
			</tr>
		<?php endif; ?>

		<?php endwhile;
	}
?>
