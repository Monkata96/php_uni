<!-- used for executing edit and delete functions and showing status of query -->
<?php
	session_start();
	require_once '../init/db.php';
	$outputJSON = array('error' => '', 'result' => '', 'status' => false);
	$error = '';
	$result = '';
	$status = false;

	$table = $_GET['table'];
	$action = $_GET['action'];
	$id = $_GET['key'];
	$outputJSON = array();

	$selector = array("guests"=>"id", "lecturers"=>"Lec_ID", "lectures"=>"L_ID", "organization"=>"O_ID");


	if ($action === "edit") {
		$sql = "SELECT * FROM ".$table." WHERE $selector[$table] = " .$id;
		$result = $conn->query($sql);

		if ($data = mysqli_fetch_assoc($result)) {
			echo json_encode($data);
		} else {
			echo "Not edited";
		}
	}

	if($action === "delete") {
		$sql = "DELETE FROM ".$table." WHERE $selector[$table] = " .$id;
		$result = $conn->query($sql);

		if ($result) {
			$_SESSION["success"] = "Record Deleted.";
		} else {
			$_SESSION["success"] = "Error deleting record.";
		}
		header("Location: {$_SERVER['HTTP_REFERER']}");
	}
 ?>
