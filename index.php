<?php
	// import head section
	include_once 'partials/head.php';
?>

<!-- CSS goes here -->
<link rel="stylesheet" href="css/home-page.css">

<?php
	include_once 'partials/header.php';
 ?>
<!-- container goes here. -->
		<header class="container">
      <h1>innovation cloud</h1>
      <p>"Connect your ideas globally"</p>
    </header>
      <div class="center">
        <a href="lecturers.php" class="arrow">
        </a>
      </div>
    </a>

<?php
	// import footer.
	include_once 'partials/footer.php';

?>
