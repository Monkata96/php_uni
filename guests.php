<?php
	require_once 'init/db.php';
	require_once 'init/dQuery.php';
	require_once 'helpers/helpers.php';


	// import head section
	include_once 'partials/head.php';
?>

<!-- CSS goes here -->
<link rel="stylesheet" href="css/guests.css">

<?php
	include_once 'partials/header.php';
	include_once 'partials/modal.php';

 ?>
<!-- container goes here. -->
<main>
	<div id="error" class="message bg-warning">
		<?=((isset($_SESSION['error']))?$_SESSION['error']:''); ?>
	</div><!-- View Errors -->
	<div id="result" class="message bg-success">
		<?=((isset($_SESSION['success']))?$_SESSION['success']:''); ?>
	</div><!-- View valid Post -->
			<h1>Тук можете да регистрирате гостите!</h1>
			<form action="core/addDatatoDB.php?form=guests" method="post">
				<div class="row input-div">
						<div class="col-2.3">
							<select name="organization" id="organization" class="form-control" placeholder="Име на организацията">
								<option value="">Избери Университет</option>
								<?php
									$result = getTableDb('organization');
									while ($organization = mysqli_fetch_assoc($result)):
								?>
									<option value="<?=$organization['O_ID'];?>"><?=$organization['Ime']; ?></option>
								<?php endwhile;	?>
							</select>
						</div>
						<div class="col-2.3">
							<select name="lecturer" id="lecturers" class="form-control">
								<option value="">Избери Преподавател</option>
							</select>
						</div>
						<div class="col-2">
							<select name="subject" id="subject" class="form-control">
								<option value="">Избери Тема</option>
								<?php
									$result = getTableDb('lectures');
									while ($subjects = mysqli_fetch_assoc($result)):
								?>
									<option value="<?=$subjects['L_ID'];?>"><?=$subjects['Tema']; ?></option>
								<?php endwhile;	?>
							</select>
						</div>
						<div class="col-1">
							<input type="number" name="guests" class="form-control" placeholder="Брой гости" min="0">
						</div>
						<div class="col-1.5">
							<input type="time" name="time" class="form-control" placeholder="Време">
						</div>
						<div class="col-2">
							<input type="date" name="date" class="form-control" placeholder="Дата" min="2017-01-02">
						</div>
						<input type="submit" name="submit" value="Submit" class="btn btn-success button">
				</div>
			</form>

				<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Организацията</th>
					<th>Преподавател</th>
					<th>Тема</th>
					<th>Брой Гости</th>
					<th>Време</th>
					<th>Дата</th>
					<th>Edit/Delete</th>

				</tr>
			</thead>
			<tbody>
				<?php
					$result = getTableDb('guests');
					$i = 1;
					while ($guests = mysqli_fetch_assoc($result)):
				?>
				<tr>
					<th scope="row"><?=$i++; ?></th>
					<td><?php
						$orgName = DbWhereClause('organization', $guests['O_ID']);
						echo $orgName['Ime'];
					 ?></td>
					<td><?php
						$LecName = DbWhereClause('lecturers', $guests['Lec_ID']);
						echo $LecName['NameOfLect'];
					 ?></td>
					<td><?php
						$LName = DbWhereClause('lectures', $guests['L_ID']);
						echo $LName['Tema'];
					 ?></td>
					<td><?=$guests['BrojG']; ?></td>
					<td><?=date('h:i A', strtotime($guests['time'])); ?></td>
					<td><?=date("jS F, Y", strtotime($guests['Date'])); ?></td>
					<td>
						<a id="edit" href="ajax/userAction.php?table=guests&action=edit&key=<?=$guests['id'];?>" class="operation fa fa-pencil-square-o" title="Edit" data-toggle="modal" data-target="#myModal"></a>
						<a id="delete" href="ajax/userAction.php?table=guests&action=delete&key=<?=$guests['id'];?>" class="operation fa fa-trash" title="Remove"></a>
					</td>
				</tr>
				<?php endwhile;	?>
			</tbody>
		</table>
</main>
<?php
	// import footer.
	include_once 'partials/footer.php';

?>
