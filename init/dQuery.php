<?php
	require_once 'db.php';
	$selector = array("guests"=>"id", "lecturers"=>"Lec_ID", "lectures"=>"L_ID", "organization"=>"O_ID");

    // Gets all data from table.

	function getTableDb($table)
	{
		global $conn;
		$sql = "SELECT * FROM $table";
		$result = $conn->query($sql);
		return $result;
	}

    // Gets data from table with given criteria.

	function DbWhereClause($table, $index)
	{
		global $conn;
		global $selector;
		$id = $selector[$table];
		$sql = "SELECT * FROM $table where $id = $index";
		$result = $conn->query($sql);
		return mysqli_fetch_assoc($result);
	}



	function magicSearch($queryString) {
		global $conn;
		global $dbName;
		$tableArray = array();
		$sql = "SHOW TABLES FROM $dbName";
		$result = $conn->query($sql);
		$resultObject = [];
		while ($tables = mysqli_fetch_assoc($result)) {
        	array_push($tableArray, $tables['Tables_in_university']);
        }

        $bigJSON = [];

        for ($i=0; $i < sizeof($tableArray); $i++) { 
        	$sql1 = "SHOW COLUMNS FROM $tableArray[$i] ";
        	$result = $conn->query($sql1);
        	$columnArray = [];
        	$columnString = '';
        	$counter = 0;
        	while ($columns = mysqli_fetch_assoc($result)) {
        		$counter++;
        		array_push($columnArray, $columns['Field']);
        		
        		if($counter == $result->num_rows) {
        			$columnString .= $columns['Field'] . ' LIKE \'' . $queryString . '\'';
        		} else{
        			$columnString .= $columns['Field'] . ' LIKE \'' . $queryString . '\' OR ';
        		}
        	}

        	$sqlQuery = "SELECT * FROM " . $tableArray[$i] ." WHERE ". $columnString;
        	$resultSearch = $conn->query($sqlQuery);

        	$fetchedData = mysqli_fetch_assoc($resultSearch);

	    		if(!is_null($fetchedData)) {
	    		 	$fetchedData['table'] = $tableArray[$i]; 
	    		 	array_push($resultObject, $fetchedData); 
	    		}       		
        }

		return $resultObject;
	}

?>
