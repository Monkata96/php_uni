<?php

    //  Initializing DB variables
	$dbhost = "localhost";
	$dbUser = "root";
	$dbPass = "";
	$dbName = "university";

    // connecting to DB.
	$conn = mysqli_connect($dbhost, $dbUser, $dbPass, $dbName);

    // Setting default charset of the DB.
	$chars="SET CHARACTER SET cp1251";
	$conn->query($chars);
	mysqli_query($conn, "SET NAMES 'utf8'");
	mysqli_query($conn, "SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

    // if unable to connect to DB, whole page will die. 
	if (!$conn) {
		die("Could not connect to the database. Reason:" .mysql_error());
	}

?>
