<?php
  require_once 'init/dQuery.php';
  // import head section
  include_once 'partials/head.php';
  require_once 'helpers/helpers.php';


?>

<!-- CSS goes here -->
<link rel="stylesheet" href="css/query.css">

<?php
  include_once 'partials/header.php';
  include_once 'partials/modal.php';

  $queryString;

  if(isset($_POST['submit'])) {
    $queryString = $_POST['queryString'];
  }


  if (!empty($queryString)) {
    $result = magicSearch($queryString);
    $_SESSION["success"] = "Record matched.";
} else {
    $_SESSION["error"] = "Your search key can't be blank!!!";
}

 ?>

<!-- CSS goes here -->
<link rel="stylesheet" href="css/home-page.css">

<?php
	include_once 'partials/header.php';
 ?>
<!-- container goes here. -->
		<main>
            <div id="error" class="message bg-warning">
        		<?=((isset($_SESSION['error']))?$_SESSION['error']:''); ?>
        	</div><!-- View Errors -->
        	<div id="result" class="message bg-success">
        		<?=((isset($_SESSION['success']))?$_SESSION['success']:''); ?>
        	</div>
        <form action="query.php" method="POST" >
            <div class="form-group d-flex justify-content-center">
              <input type="text" name="queryString" class="form-control col-4 form-row" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your query">
            </div>
            <div class="form-group d-flex justify-content-center">
              <input type="submit" name="submit" value="Submit" class="btn btn-success button">
            </div>
        </form>

        <div class="result">

          <?php
              if (isset($_POST['submit'])) {
                 $result = magicSearch($queryString);
                  for ($i=0; $i < sizeof($result) ; $i++) {
                       $currentArray = $result[$i];
                       $last = end($currentArray);
                       echo '<div class="data-div">
                         <div class="header-div">
                          <span> This data belongs to ' . capitalizeNstrong($last) . ' Table </span></div>
                       ';
                      foreach ($currentArray as $key => $value) {
                          //echo $key ."  =>  ". $value ." <br>";
                          if ($key != 'table') {
                            ?>
                             <div class="wrapper">
                                <div class="data-left">
                                  <span><?=getColumnName($key); ?></span>
                                </div>
                                <div class="data-right">
                                  <span>
                                      <?php
                                        if($key == 'Date') {
                                            echo date("jS F, Y", strtotime($value));
                                        } else if($key == 'time') {
                                            echo date("h:i A", strtotime($value));
                                        } else if($key == 'DuljinaL') {
                                            echo getTime($value);
                                        } else {
                                            echo $value;
                                        }
                                       ?>
                                  </span>
                                </div>
                            </div>

                            <?php
                          }

                      }

                      echo '<div class="clearfix"> </div></div>';
                  }
              }
          ?>
        </div>
    </main>
<?php
	// import footer.
	include_once 'partials/footer.php';

?>
