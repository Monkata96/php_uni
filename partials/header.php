<!-- includes navigation -->
</head>
<body>
	<header>
      <div class="row" id="navigation">
        <nav class="navbar navbar-toggleable-md navbar-light bg-faded col-lg-12">
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item ">
                <a class="nav-link" href="index.php">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="organizations.php">Organization</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="lecturers.php">Lecturers</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="lectures.php">Lectures</a>
              </li>
                <li class="nav-item">
                <a class="nav-link" href="guests.php">Guests</a>
              </li>
              <li>
                <li class="nav-item">
                <a class="nav-link" href="query.php">Queries</a>
              </li>
            </ul>
          </div>
        </nav>
    </div>
</header>
