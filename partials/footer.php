<!-- Footer starts here -->
	<footer class="footer">
  <div class="social">
      <a href="https://facebook.com/ondrej.p.barta" target="_blank" class="link facebook" target="_parent"><span class="fa fa-facebook-square"></span></a>
      <a href="https://twitter.com/PageOnlineXS" target="_blank" class="link twitter" target="_parent"><span class="fa fa-twitter"></span></a>
      <a href="https://plus.google.com/+OndřejBárta-Otaku" target="_blank" class="link google-plus" target="_parent"><span class="fa fa-google-plus-square"></span></a>
      <a href="https://www.youtube.com/" target="_blank"class="link youtube" target="_parent"><span class="fa fa-youtube-play"></span></a>
      <a href="https://bitbucket.org/product" target="_blank"class="link bitbucket" target="_parent"><span class="fa fa-bitbucket"></span></a>
  </div>
  <div class="row row-flex">
    <div class="col-lg-6 col-sm-7 wow fadeInLeft">
        <div class="contact-info-box address clearfix">
            <h3><i class=" icon-map-marker"></i>Address:</h3>
            <span>Broadway, New York. </span>
          </div>
          <div class="contact-info-box phone clearfix">
            <h3><i class="fa-phone"></i>Phone:</h3>
            <span>0-700-///-///</span>
          </div>
          <div class="contact-info-box email clearfix">
            <h3><i class="fa-pencil"></i>email:</h3>
            <span>innocloud@gmail.com</span>
          </div>
        <div class="contact-info-box hours clearfix">
            <h3><i class="fa-clock-o"></i>Hours:</h3>
            <span><strong>Monday - Thursday:</strong> 10am - 6pm<br><strong>Friday:</strong>10am - 4pm<br><strong>Saturday - Sunday:</strong> Who works over the weekend?</span>
          </div>
      </div>
  </div>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
crossorigin="anonymous"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="https://npmcdn.com/bootstrap@4.0.0-alpha.5/dist/js/bootstrap.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script src="javascript/main.js"></script>
<script src="javascript/UrlParams.js"></script>
<script src="https://use.fontawesome.com/e8c1bcee8d.js"></script>
</body>
</html>

<?php
	if (isset($_SESSION['error'])) {
		unset($_SESSION['error']);
	}
	if (isset($_SESSION['success'])) {
		unset($_SESSION['success']);
	}
?>

<!-- footer ends here. -->
